**KC-Addon**

Important: Set the environment variable `KC_ADDON_CLOUDFLARE_DISABLED` to `true` if you're not using Cloudflare.

Also, don't forget to initialize `dont-reload/globalSalt` with a random string and `touch` `dont-reload/dnswl`. You may also want to do: `cp config/inlineImages.json.example config/inlineImages.json`.

Requirements:

- LynxChan 2.4.x: https://gitgud.io/LynxChan/LynxChan/tree/2.4.x
- KohlNumbra: https://gitgud.io/Tjark/KohlNumbra
- Create dont-reload/globalSalt (random) and config/inlineImages.json (copy config/inlineImages.json.example)
- mimetypes (perl); libfile-mimeinfo-perl on Debian => used as a fallback for some files that are not recognized by file(1)
- Kohlcash: https://gitgud.io/Tjark/Kohlcash (optional)
