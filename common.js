'use strict';

var domCommon = require('../../engine/domManipulator').common;
var db = require('../../db');
var threads = db.threads();
var posts = db.posts();

var projectionBlock = {
  projection : {
    _id : 0,
    id : 1,
    name : 1,
    flag : 1,
    files : 1,
    email : 1,
    postId : 1,
    message : 1,
    subject : 1,
    boardUri : 1,
    markdown : 1,
    creation : 1,
    threadId : 1,
    flagName : 1,
    flagCode : 1,
    signedRole : 1,
    innerCache : 1,
    banMessage : 1,
    lastEditTime : 1,
    lastEditLogin : 1,
    alternativeCaches : 1,
    sage : 1
  }
};

exports.getPostsFromQuery = function(query, callback) {

  threads.find(query, projectionBlock).sort({
    creation : -1
  }).toArray(function gotThreads(error, foundThreads) {

    if (error) {
      callback(error);
    } else {

      // style exception, too simple
      posts.find(query, projectionBlock).sort({
        creation : -1
      }).toArray(function gotPosts(error, foundPosts) {

        if (error) {
          callback(error);
        } else {

          callback(null, foundPosts.concat(foundThreads).sort(function(a, b) {
            return b.creation - a.creation;
          }));

        }

      });
      // style exception, too simple

    }

  });

};

exports.buildPostList = function(postings, language) {

  var postsContent = '';

  var operations = [];

  for (var i = 0; i < postings.length; i++) {

    var postContent = domCommon.getPostCellBase(postings[i]);

    postContent += domCommon.getPostInnerElements(postings[i], true, language, operations);

    postsContent += postContent + '</div>';
  }

  domCommon.handleOps(operations);

  return postsContent;

};
