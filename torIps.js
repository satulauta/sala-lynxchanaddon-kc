'use strict';

var fs = require('fs');
var dns = require('dns');
var crypto = require('crypto');
var logger = require('../../logger');
var bypass = require('../../engine/bypassOps');
var torOps = require('../../engine/torOps');
var locationOps = require('../../engine/locationOps');
var formOps = require('../../engine/formOps');
var versatile = require('../../engine/modOps').ipBan.versatile;
var lang = require('../../engine/langOps').languagePack;
var appealBan = require('../../form/appealBan');
var blockedASN = __dirname + '/dont-reload/blockedASN';
var compiledASNs = __dirname + '/../../locationData/compiledASNs';
var torPassAllowed;
var torLevel;
var torAllowed;
var bypassAllowed;

exports.loadSettings = function() {

  var settings = require('../../settingsHandler').getGeneralSettings();

  torLevel = settings.torPostingLevel;
  torAllowed = torLevel > 0;
  bypassAllowed = settings.bypassMode > 0;
  torPassAllowed = bypassAllowed && torAllowed;

};

exports.initIpAssignment = function() {

  /*
  var originalUseBypass = bypass.useBypass;

  bypass.useBypass = function(bypassId, req, callback) {

    req.bypassId = bypassId;

    originalUseBypass(bypassId, req, callback);

  };
  */

  var originalGetIp = logger.ip;

  var slightlyOriginalGetIp = function(req) {
    if (req.cachedIp2) {
      return req.cachedIp2;
    } else {
      req.cachedIp2 = logger.convertIpToArray(logger.getRawIp(req));

      return req.cachedIp2;
    }
  };

  logger.ip = function(req, real) {

    if ((req.isTor || req.tempTor) && req.bypassId) {

      if (real) {
        return slightlyOriginalGetIp(req);
      }

      if (req.cachedIp) {
        return req.cachedIp;
      }

      var ipNumber = crypto.createHash('md5').update(req.bypassId)
          .digest('hex');

      var toRet = [];

      toRet[0] = 127;

      for (var i = 1; i < 4; i++) {
        toRet[i] = parseInt(ipNumber.substring(i * 2, (i * 2) + 2), 16);
      }

      req.cachedIp = toRet;

      return toRet;

    } else {

      if (real) {
        return null;
      }

      return originalGetIp(req);

    }

  };

};

exports.checkASN = function(req, callback) {

  var temp = fs.readFileSync(blockedASN).toString().split('\n');

  var blockedASNList = [];

  for (var i = 0; i < temp.length; i++) {

    var trimmed = temp[i].trim();

    if (trimmed) {
      blockedASNList.push(+trimmed);
    }

  }

  logger.binarySearch({
    ip : locationOps.ipToInt(logger.convertIpToArray(logger.getRawIp(req)))
  }, compiledASNs, 8, function compare(a, b) {
    return a.ip - b.ip;
  }, function parse(buffer) {
    return {
      ip : buffer.readUInt32BE(0),
      asn : buffer.readUInt32BE(4)
    };
  }, function gotIp(error, data) {

    if (data) {
      req.tempTor = blockedASNList.indexOf(data.asn) >= 0;

      if (req.tempTor) {
        delete req.cachedIp;
      }

      req.ASN = req.tempTor;

    }

    callback(error, req);

  }, true);

};

exports.getLookupPrefix = function(ip) {

  if (ip.length == 4) {

    return ip.reduceRight((a, b) => {
      return a + '.' + b;
    }) + '.';

  } else {

    var tmp = '';
    for (var i = 0; i < ip.length; i++) {
      var hex = ('0' + ip[i].toString(16)).slice(-2);
      tmp = hex[1] + '.' + hex[0] + '.' + tmp;
    }
    return tmp;

  }

};

exports.checkDNSBL = function(req, callback) {

  var ip = logger.ip(req);

  var prefix = exports.getLookupPrefix(ip);

  var addresses = [
    'dnsbl.dronebl.org',
    'torexit.dan.me.uk'
  ];

  if (ip.length == 4)
    addresses.pop();

  exports.resolveDNSBL(req, prefix, addresses, null, callback);

};

exports.resolveDNSBL = function(req, prefix, addresses, index, callback) {

  index = index || 0;

  if (index < addresses.length) {

    dns.resolve(prefix + addresses[index], function(error, data) {

      if ((error && error.code !== 'ENOTFOUND') || data) {

        var val = !!data;

        req.tempTor = val;

        if (req.tempTor) {
          delete req.cachedIp;
        }

        req.DNSBL = req.tempTor;

        callback(error, req);

      } else {
        exports.resolveDNSBL(req, prefix, addresses, ++index, callback);
      }

    });

  } else {
    exports.checkASN(req, callback);
  }

};

exports.initBanCheck = function() {

  var originalCheckForBan = versatile.checkForBan;

  versatile.checkForBan = function(req, boardUri, callback) {

    originalCheckForBan(req, boardUri, function(error, ban, bypassable) {

      req.isTor = req.tempTor;
      delete req.tempTor;

      if (req.isTor && (!torAllowed || (!req.bypassed && torLevel < 2))) {
        callback(torPassAllowed ? null : lang(req.language).errBlockedTor,
            null, torPassAllowed);
      } else {
        callback(error, ban, bypassable);
      }

    });

  };

  var originalGetActiveBan = versatile.getActiveBan;

  versatile.getActiveBan = function(ip, boardUri, callback) {

    originalGetActiveBan(ip, boardUri, (a, b) => {
      callback(a, b);
    });

  };

  var originalMarkAsTor = torOps.markAsTor;

  torOps.markAsTor = function(req, callback) {

    originalMarkAsTor(req, function(error) {

      if (!req.isTor) {
        return exports.checkDNSBL(req, callback);
      }

      req.tempTor = req.isTor;
      delete req.cachedIp;
      delete req.isTor;

      callback(error, req);

    });

  };

  var originalAppealBan = appealBan.process;

  appealBan.process = function(req, res) {

    var cookies = formOps.getCookies(req);

    req.bypassId = cookies.bypass;

    torOps.markAsTor(req, function marked(error) {

      if (error) {
        formOps.outputError(error, 500, res, req.language, formOps.json(req));
      } else {
        originalAppealBan(req, res);
      }

    });

  };

};

exports.init = function() {

  exports.initIpAssignment();

  exports.initBanCheck();

};
